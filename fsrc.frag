#version 330
uniform sampler2D sTexture;
in vec2 vTexture;
out vec4 fragColor;

void main(void)
{
    fragColor = texture2D(sTexture,vTexture);
}
